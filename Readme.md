# Webkuta #

## Unofficial Release Note ##

This is a unofficial release made only for testing purposes.

## Description ##

Webkuta is a GeoIP-location application used to draw IPv4-address location on the map.
The application uses following components to archive the goal: 

- [GeoIP2-php](https://github.com/maxmind/GeoIP2-php)
- [Google Maps API](https://developers.google.com/maps/)
- [Google Geocoding API](https://developers.google.com/maps/documentation/geocoding/)
- [GeoLite2 databases](http://dev.maxmind.com/geoip/geoip2/geolite2/).

## Installation ##

### Install Database ###

Download and copy binary format GeoLite2 City database in the project's src -folder.
Do not change the name of database, unless you are willing to change the code of application.


### Define Your Dependencies ###

This application can't be found from the Packagist, 
so you can't yet install the application with the Composer.
Anyway, you need the Composer to install dependencies of the application.


### Install Composer ###

Run in the project root:

```
curl -s http://getcomposer.org/installer | php
```

### Install Dependencies ###

Run in the project root:

```
php composer.phar install

```

## Copyright and License ##

This application is Copyright (c) 2014 by Lorvi Anton.

This is free application, licensed under the Apache License, Version 2.0.