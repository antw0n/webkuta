<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
        <meta name="description" content="IP-address geolocator">
        <meta name="keywords" content="webkuta, location, service">
        <meta name="author" content="Anton Lorvi">
        <meta charset="UTF-8">

        <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.3.0/pure-min.css"/>
        <link rel="stylesheet" type="text/css" href="style.css">
        
        <script src="http://www.codehelper.io/api/ips/?js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script type="text/javascript" src="../src/Functions.js"></script>
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
        <script type="text/javascript" src="../src/GoogleMaps.js"></script>
        
        <title>Webkuta - Location service</title>
    </head>
    <body onload="initialize()">
        <div id="top-bar">
            <form class="pure-form">
                <table id="search_table">
                    <tr>   
                        <td>
                            <div id="name" >Webkuta - Location service</div>
                        </td>
                        <td>
                            <input class="input" id="address" type="text" >
                        </td>
                        <td>
                            <input id="search" class="pure-button pure-button-small" type="button" value="SEARCH" onclick="codeAddress()">
                        </td>
                    </tr>
                </table>
            </form>
        </div>
        <div id="map-canvas"></div>
        <div id="footer"> 
            <div id="text">Designed by Anton Lorvi. This product includes GeoLite data created by MaxMind, available from
                <a href="http://www.maxmind.com">http://www.maxmind.com</a>.
            </div>
        </div>
    </body>
</html>

