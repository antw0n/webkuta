$(document).ready(function() {
    $("#top-bar").mouseover(function() {
        $("#top-bar").fadeTo(600, .9);
    });
    $("#top-bar").click(function() {
        $("#top-bar").fadeTo(600, .9);
    });
    $("#map-canvas").click(function() {
        $("#top-bar").fadeTo(200, .2);
    });
    $("#footer").mouseover(function() {
        $("#footer").fadeTo(600, .9);
        $("#footer").css("background-color", "rgb(50, 50, 50)");
    });
    $("#map-canvas").click(function() {
        $("#footer").css("background", "none").fadeTo(200, .6);
    });
});
$(document).keypress(function(e) {
    if (e.which === 13) {
        $("#search").click();
        e.preventDefault();
    }
});