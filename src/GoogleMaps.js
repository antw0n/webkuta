var geocoder;
var map;

function initialize() {
    codeAddress();
}

function codeAddress() {

    var address = document.getElementById('address').value;

    if (address === null || address === undefined || address === "") {
        address = codehelper_ip.IP;
    }

    $.ajax(
            {
                url: '../src/AddressBuilder.php',
                data: {'address': address},
                type: 'post',
                success: function(output)
                {
                    coordinates = output;

                    geocoder = new google.maps.Geocoder();
                    geocoder.geocode({
                        'address': coordinates
                    }, function(results, status) {
                        if (status === google.maps.GeocoderStatus.OK) {
                            var myOptions = {
                                zoom: 8,
                                center: results[0].geometry.location,
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            }
                            map = new google.maps.Map(document.getElementById("map-canvas"), myOptions);

                            var marker = new google.maps.Marker({
                                map: map,
                                position: results[0].geometry.location
                            });
                        }
                    });

                }
            }
    );
}





