<?php

require_once '../vendor/autoload.php';

use GeoIp2\Database\Reader;

class IpHandler {

    private $reader = null;
    private $record = null;

    public function getRecord() {
        return $this->record;
    }

    public function __construct() {
        $this->reader = new Reader('GeoLite2-City.mmdb');
    }

    public function getLocationDataByIp($ip) {
        $this->record = $this->reader->city($ip);
    }

    public function getCoordinates() {
        echo (float) $this->record->location->latitude;
        echo ', ';
        echo $this->record->location->longitude;
    }

    public function printOut() {
        var_dump($this->record);
    }

}

